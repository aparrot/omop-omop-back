import pandas as pd
import pysolr
from pandas._testing import assert_frame_equal

from pysolrwrapper.core import SolrQuery
from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient
from pysolrwrapper.loader_solr.pg_to_solr import pg_to_solr_query
from pysolrwrapper.utils import resource_path_root
from tests.pysolrwrapper.conftest import SOLR_DATA


def test_susana_sample_loaded(containers):
    assert (
        len(pd.read_sql("select * from susana.concept", containers.pg_client.conn).index)
        == 100
    )


def test_susana_sample_loaded(containers):
    assert (
        len(
            pd.read_sql(
                "select * from susana.mapper_user", containers.pg_client.conn
            ).index
        )
        == 1
    )


def test_format_project_function(susana_con):
    sql = """\
   select
   format_project('hello - world', 'nothing') as a,
   format_project('world', 'hello') as b;
   """
    goal = pd.DataFrame({"a": ["hello - world"], "b": ["hello - world"]})
    assert_frame_equal(goal, pd.read_sql(sql, susana_con))


def test_format_test_function(containers):
    sql = """\
   select
   susana.format_text('hell|o - w\torld', '|', '\t') as a,
   susana.format_text('hello - world', '|', '\t') as b;
   """
    goal = pd.DataFrame({"a": ["hello - world"], "b": ["hello - world"]})
    assert_frame_equal(goal, pd.read_sql(sql, containers.pg_client.conn))


def test_transform_pg_to_solr(susana_sample_con, solr, solr_client):
    pg_client = PostgresClient(susana_sample_con)
    file = str(resource_path_root() / "data" / "solr.csv")
    pg_client.bulk_export_solr(pg_to_solr_query(), file)
    solr_core = "susana_core"
    solr_client.solr_bulk_load(solr_core, f"{SOLR_DATA}/solr.csv")
    solr = pysolr.Solr(f"{solr_client.conf}/{solr_core}", auth=solr_client.auth)
    result = solr.search("*:*")
    assert result.hits == 100


def test_transform_pg_to_solr_with_atomic_update(susana_sample_con, solr, solr_client):
    pg_client = PostgresClient(susana_sample_con)
    file = str(resource_path_root() / "data" / "solr.csv")
    pg_client.bulk_export_solr(pg_to_solr_query(), file)
    solr_core = "susana_core"
    solr_client.solr_bulk_load(solr_core, f"{SOLR_DATA}/solr.csv")
    solr = SolrQuery(
        host=solr_client.conf.host,
        port=solr_client.conf.port,
        core=solr_core,
        auth=solr_client.auth,
    )
    solr.set_is_mapped("2002025431", is_standard=False)
    psolr = pysolr.Solr(f"{solr_client.conf}/{solr_core}", auth=solr_client.auth)
    result = psolr.search(
        'id:2002025431 AND is_mapped:"OSIRIS - NS" AND local_map_number:1'
    )
    assert result.hits == 1

    solr.unset_is_mapped("2002025431")
    result = psolr.search(
        'id:2002025431 AND is_mapped:"OSIRIS - EMPTY" AND local_map_number:0'
    )
    assert result.hits == 1


def test_bulk_load_pandas(containers):
    pg_client = containers.pg_client
    print(pg_client.run("create table foo (a int, b text)"))
    df = pd.DataFrame({"a": [1], "b": ["foo"]})
    pg_client.bulk_load_pandas(df, "foo")

    assert_frame_equal(df, pd.read_sql("select * from foo", containers.pg_client.conn))
