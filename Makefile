.DEFAULT_GOAL := help
.PHONY: help docs format start stop

help:   ## Susana commands
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

format:  ## format python code
	black app asynchron .

init:  ## initialize the project with various env
	echo "api_hostname=`hostname -I | awk '{print $$1}'`" > .env
	echo "UID=`id -u`" >> .env

test:
	tox

docs: ## build the doc and serve locally for review
	mkdocs build --clean --verbose
	mkdocs serve

start: ## start the local docker stack
	docker-compose down && docker-compose up -d

stop: ## stop the local docker stack
	docker-compose down

release.patch: ## stop the local docker stack
	git checkout master
	git fetch --force --tags
	bump2version --allow-dirty patch
	towncrier build --yes --version $$(bump2version --allow-dirty --dry-run --list patch | grep current_version | sed -r s,"^.*=",,)
	release-it -i patch

release.minor: ## stop the local docker stack
	git checkout master
	git fetch --force --tags
	bump2version --allow-dirty minor
	towncrier build --yes --version $$(bump2version --allow-dirty --dry-run --list patch | grep current_version | sed -r s,"^.*=",,)
	release-it minor

release.major: ## stop the local docker stack
	git checkout master
	git fetch --force --tags
	bump2version --allow-dirty major
	towncrier build --yes --version $$(bump2version --allow-dirty --dry-run --list patch | grep current_version | sed -r s,"^.*=",,)
	release-it major
