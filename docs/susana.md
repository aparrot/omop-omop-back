Introduction
============
This project is the backend part of [omop-mapper.fgh.ovh](http://omop-mapper.fgh.ovh)

How to install ?
=================
# database
- path for the csv : ln -s local_csv_file csv/omop
- make postgres-create to create de db 

- set db with config.py file in app conf_files: postgres auth
``` python
# To generate a new secret key:
# >>> import random, string
# >>> result = "".join([random.choice(string.printable) for _ in range(24)])
SECRET_KEY = result

SQLALCHEMY_TRACK_MODIFICATIONS = False
PROPAGATE_EXCEPTIONS = True
JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

POSTGRES = {
    'user': 'omop-mapper',
    'pwd': 'omop-mapper',
    'db': 'omop-mapper',
    'host': 'localhost',
    'port': '5432',

}
SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pwd)s\
@%(host)s:%(port)s/%(db)s' % POSTGRES
```

# run app
- dev mode (no synchronisation with solr, no need to install solr)
	- virtualenv -p python3.7 venv
	- source venv/bin/activate
	- pip install -r app/equirements.txt
	- mv app/server.py .
	- mv conf_files/config.py app/.
	- python server.py

- production mode : uwsgi/proxy/flask (should install solr before)
	- docker-compose build
	- docker-compose up

Languages
=========
See ISO_OMOP_LANGUAGES in share/macros.py

Frontend
========
The frontend part is [here](https://framagit.org/interchu/concept_mapper_front)

TODO
====
Put Solr and Livy in docker

Remark : Create Standardized vocabulary tables
==============================================
- https://github.com/MIT-LCP/mimic-omop/tree/master/omop/build-omop/postgresql
- https://github.com/OHDSI/CommonDataModel/tree/master/PostgreSQL
