import logging
import time

from asynchron.jobs.email import send_email
from conf_files.config import (
    JOB_FREQUENCY_IN_SECONDS,
)

logging.getLogger().setLevel(level=logging.WARN)

email_jobs = [
    "USER_REGISTRATION_EMAIL",
    "USER_ACTIVATION_EMAIL",
    "LOGIN_SUCCESS_EMAIL",
    "LOGIN_FAILURE_EMAIL",
]

while True:
    time.sleep(JOB_FREQUENCY_IN_SECONDS)

    # 0: TO BE DONE
    # 1: DOING
    # 2: DONE
    # 3: ERROR

    # EMAIL JOBS
    send_email(email_jobs)
