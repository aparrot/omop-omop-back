#!/usr/bin/env bash

PYTHON_JOB=$1
/usr/bin/tor &
echo -n "$!" > /tmp/PID
python "$PYTHON_JOB"
