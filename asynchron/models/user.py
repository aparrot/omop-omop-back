from datetime import datetime, timedelta

from sqlalchemy import TIMESTAMP, Column, Integer, String

from asynchron.db import Base, session


class UserModel(Base):
    __tablename__ = "mapper_user"
    __table_args__ = {"schema": "susana"}

    id = Column("m_user_id", Integer, primary_key=True, autoincrement=True)

    m_username = Column(String(255), nullable=False, unique=True)
    m_password = Column(String(255), nullable=False)
    m_firstname = Column(String(255))
    m_lastname = Column(String(255))
    m_email = Column(String(255), unique=True)
    m_address = Column(String(255))
    m_invalid_reason = Column(String(1))
    m_is_admin = Column(Integer)
    m_signin_datetime = Column(TIMESTAMP(), default=datetime.now())
    m_valid_start_datetime = Column(TIMESTAMP(), default=datetime.now())
    m_valid_end_datetime = Column(
        TIMESTAMP(), default=datetime.now() + timedelta(weeks=5200)
    )

    def __init__(
        self,
        m_username,
        m_password,
        m_firstname,
        m_lastname,
        m_email,
        m_address,
        m_is_admin=0,
        m_valid_end_datetime=None,
        m_invalid_reason="D",
    ):

        m_is_admin = 1 if m_is_admin == "Admin" else 0
        self.m_username = m_username
        self.set_password(m_password)
        self.m_firstname = m_firstname.title()
        self.m_lastname = m_lastname.title()
        self.m_email = m_email.lower()
        self.m_address = m_address.lower()
        self.m_is_admin = m_is_admin
        self.m_invalid_reason = m_invalid_reason
        if m_valid_end_datetime != None:
            self.m_valid_end_datetime = m_valid_end_datetime

    def upserting(self):
        session.add(self)
        session.commit()

    def delete_from_db(self):
        session.delete(self)
        session.commit()

    @classmethod
    def find_by_id(cls, m_user_id):
        return session.query(cls).filter_by(id=m_user_id).first()

    @classmethod
    def find_by_username(cls, m_username):
        return session.query(cls).filter_by(m_username=m_username).first()

    @classmethod
    def exist_email(cls, m_email):
        return session.query(cls).filter_by(m_email=m_email).first()
