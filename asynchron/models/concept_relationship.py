from datetime import datetime, timedelta

from sqlalchemy import TIMESTAMP, Column, Date, Integer, Text

from asynchron.db import Base, session


class ConceptRelationModel(Base):
    __tablename__ = "concept_relationship"
    __table_args__ = {"schema": "susana"}

    m_concept_relationship_id = Column(Integer, primary_key=True)
    concept_id_1 = Column(Integer)
    concept_id_2 = Column(Integer)
    relationship_id = Column(Text())
    valid_start_date = Column(Date(), default=datetime.now())
    valid_end_date = Column(Date(), default=datetime.now() + timedelta(weeks=5200))
    invalid_reason = Column(Text())
    m_user_id = Column(Integer)
    m_algo_id = Column(Integer)
    m_modif_start_datetime = Column(TIMESTAMP(), default=datetime.now())  # CURRENT_DATE
    m_modif_end_datetime = Column(TIMESTAMP(), default=None)
    m_mapping_rate = Column(Integer())
    m_mapping_comment = Column(Text())

    def __init__(
        self,
        concept_id_1,
        concept_id_2,
        relationship_id,
        m_user_id,
        m_algo_id,
        invalid_reason=None,
        valid_end_date=None,
        m_modif_end_datetime=None,
        m_mapping_rate=0,
        m_mapping_comment="",
    ):
        self.concept_id_1 = concept_id_1
        self.concept_id_2 = concept_id_2
        self.relationship_id = relationship_id
        if valid_end_date != None:
            self.valid_end_date = valid_end_date
        self.invalid_reason = invalid_reason
        self.m_user_id = m_user_id
        self.m_algo_id = m_algo_id
        if m_modif_end_datetime != None:
            self.m_modif_end_datetime = m_modif_end_datetime
        self.m_mapping_rate = m_mapping_rate
        self.m_mapping_comment = m_mapping_comment

    # m_algo_id = []
    @classmethod
    def find_by_id(
        cls, concept_id_1, concept_id_2=None, relationship_id=None, m_algo_id=None
    ):
        query_set = None
        if concept_id_1 != None:
            query_set = session.query(cls).filter_by(concept_id_1=concept_id_1)
        if concept_id_2 != None:
            if query_set != None:
                query_set = query_set.filter_by(concept_id_2=concept_id_2)
            else:
                query_set = session.query(cls).filter_by(concept_id_2=concept_id_2)
        if relationship_id != None:
            if query_set != None:
                query_set = query_set.filter_by(relationship_id=relationship_id)
            else:
                query_set = session.query(cls).filter_by(relationship_id=relationship_id)
        if m_algo_id != None:
            if query_set != None:
                query_set = query_set.filter(cls.m_algo_id.in_(m_algo_id))
            else:
                query_set = session.query(cls).filter(cls.m_algo_id.in_(m_algo_id))

        if query_set != None:
            query_set = query_set.order_by(cls.m_modif_start_datetime.desc())
        return query_set.all()

    @classmethod
    def find_by_concept_relationship_id(cls, m_concept_relationship_id):
        return (
            session.query(cls)
            .filter_by(m_concept_relationship_id=m_concept_relationship_id)
            .first()
        )

    def upserting(self):
        session.add(self)
        session.commit()
