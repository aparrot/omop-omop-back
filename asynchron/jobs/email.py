# https://arjunkrishnababu96.gitlab.io/post/send-emails-using-code/

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template


from conf_files.config import EMAIL_ADDRESS, EMAIL_PASSWORD, EMAIL_ADMIN_ADDRESS, EMAIL_SMTP_PORT, EMAIL_SMTP_HOST
from asynchron.models.job import JobModel
from asynchron.models.user import UserModel


def read_template(filename):
    """
    Returns a Template object comprising the contents of the
    file specified by filename.
    """

    with open(filename, "r", encoding="utf-8") as template_file:
        template_file_content = template_file.read()

    return Template(template_file_content)


def send_user_registration_email(user, server):
    msg = MIMEMultipart()  # create a message

    message_template = read_template("/project/asynchron/jobs/template/user_registration.txt")
    message = message_template.substitute(PERSON_USERNAME=user.m_username)

    # setup the parameters of the message
    msg["From"] = EMAIL_ADDRESS
    msg["To"] = user.m_email
    msg["Subject"] = "Susana.interhop.org Registration"

    # add in the message body
    msg.attach(MIMEText(message, "plain"))
    server.send_message(msg)


def send_admin_registration_email(user, server):
    msg = MIMEMultipart()  # create a message

    message_template = read_template("/project/asynchron/jobs/template/admin_registration.txt")
    message = message_template.substitute(PERSON_USERNAME=user.m_username, PERSON_EMAIL=user.m_email)

    # setup the parameters of the message
    msg["From"] = EMAIL_ADDRESS
    msg["To"] = EMAIL_ADMIN_ADDRESS
    msg["Subject"] = "New user on Susana!"

    # add in the message body
    msg.attach(MIMEText(message, "plain"))
    server.send_message(msg)


def send_email(jobs_filter):
    jobs = JobModel.jobs_to_process(jobs_filter)
    print("jobs/email.py RUNNING")

    if jobs:

        if EMAIL_SMTP_HOST == 'localhost':
            server = smtplib.SMTP(EMAIL_SMTP_HOST, int(EMAIL_SMTP_PORT))
        else:
            server = smtplib.SMTP(EMAIL_SMTP_HOST, int(EMAIL_SMTP_PORT))
            server.ehlo()  # On utilise la commande EHLO
            server.starttls()  # On appelle la fonction STARTTLS
            server.login(str(EMAIL_ADDRESS), str(EMAIL_PASSWORD))

        for job in jobs:

            print("email processing - run on user : ", job.m_concept_id)

            if job.m_job_type_id == "USER_REGISTRATION_EMAIL":
                user = UserModel.find_by_id(job.m_concept_id)
                if user:
                    try:
                        send_user_registration_email(user, server)
                        send_admin_registration_email(user, server)
                        job.m_status_id = 2
                        job.upserting()
                    except:
                        job.m_status_id = 3
                        job.upserting()

            elif job.m_job_type_id == "LOGIN_SUCCESS_EMAIL":
                user = UserModel.find_by_id(job.m_concept_id)
                if user:
                    try:
                        job.m_status_id = 2
                        job.upserting()
                    except:
                        job.m_status_id = 3
                        job.upserting()

            elif job.m_job_type_id == "LOGIN_FAILURE_EMAIL":
                user = UserModel.find_by_id(job.m_concept_id)
                if user:
                    try:
                        job.m_status_id = 2
                        job.upserting()
                    except:
                        job.m_status_id = 3
                        job.upserting()

        # Terminate the SMTP session and close the connection
        server.quit()
