from asynchron.models.job import JobModel


def spark_processing(jobs_filter, spark_session):
    jobs = JobModel.jobs_to_process(jobs_filter)
    if len(jobs) > 1000:
        jobs = jobs[0:1000]
    jobs_id = [job.m_concept_id for job in jobs]
    jobs_id = list(set(jobs_id))
    print("jobs/spark.py running")

    if len(jobs_id) > 0:
        print("spark_processing - run on : ", jobs_id)
        try:
            spark_session.run(",".join([str(x) for x in set(jobs_id)]))
            for job in jobs:
                job.m_status_id = 2
                job.upserting()

        except:
            for job in jobs:
                job.m_status_id = 3
                job.upserting()
