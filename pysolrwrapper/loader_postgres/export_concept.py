from pathlib import Path

import click

from conf_files.config import POSTGRES
from pysolrwrapper.loader_postgres.postgres_utils import (
    PostgresClient,
    PostgresConf,
)


@click.command()
@click.option(
    "--export-folder",
    type=click.Path(exists=True),
    help="Folder where csv are exported",
    required=True,
)
@click.option(
    "--project",
    help="Project to export",
    required=True,
)
def export(export_folder: Path, project: str):
    """Upgrade the susana concepts with a new athena export"""
    pg_client = PostgresClient.from_conf(PostgresConf(**POSTGRES))
    conn = pg_client.conn
    pg_client = PostgresClient(conn)

    concept = f"""
        SELECT
          concept_id
        , concept_name
        , domain_id
        , vocabulary_id
        , concept_class_id
        , standard_concept
        , concept_code
        , valid_start_date
        , valid_end_date
        , invalid_reason
        from susana.concept
        where TRUE
        AND m_project_id IN (
        	select m_project_id
        	from susana.mapper_project
        	where lower(m_project_type_id) = lower('{project}')
        )
    """
    pg_client.bulk_export(concept, str(export_folder / Path("concept.csv")))

    concept_synonym = f"""
        SELECT
          s.concept_id
        , s.concept_synonym_name
        , s.language_concept_id
        from susana.concept_synonym  s
        join susana.concept using (concept_id)
        where TRUE
        AND m_project_id IN (
        	select m_project_id
        	from susana.mapper_project
        	where lower(m_project_type_id) = lower('{project}')
        )
    """
    pg_client.bulk_export(concept_synonym, str(export_folder / Path("concept_synonym.csv")))

    concept_relationship = f"""
        select
          r.concept_id_1
        , r.concept_id_2
        , r.relationship_id
        , r.valid_start_date
        , r.valid_end_date
        , r.invalid_reason
        from susana.concept_relationship r
        join susana.concept c on (c.concept_id = r.concept_id_1)
        where TRUE
        AND m_project_id IN (
        	select m_project_id
        	from susana.mapper_project
        	where lower(m_project_type_id) = lower('$PROJECT')
        )
    """
    pg_client.bulk_export(concept_relationship, str(export_folder / Path("concept_relationship.csv")))

    vocabulary = f"""
        select
          vocabulary_id
        , vocabulary_name
        , vocabulary_reference
        , vocabulary_version
        , vocabulary_concept_id
        from susana.vocabulary
        where TRUE
        AND m_project_id IN (
        	select m_project_id
        	from susana.mapper_project
        	where lower(m_project_type_id) = lower('{project}')
        )
    """
    pg_client.bulk_export(vocabulary, str(export_folder / Path("vocabulary.csv")))
