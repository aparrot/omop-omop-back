from dataclasses import dataclass
from typing import Optional

import requests
from requests.auth import HTTPBasicAuth


@dataclass
class SolrConf:
    host: str
    port: int
    core: str
    user: Optional[str] = None
    password: Optional[str] = None

    def __repr__(self) -> str:
        return f"http://{self.host}:{self.port}/solr"


class SolrClient:
    def __init__(self, conf: SolrConf):
        self.session = requests.sessions.session()
        self.conf = conf
        self.auth = auth = (
            HTTPBasicAuth(conf.user, conf.password)
            if conf.password and conf.user
            else None
        )
        self.session.auth = auth

    def solr_query(self, endpoint):
        res = self.session.get(f"{str(self.conf)}" + endpoint)
        if res.status_code != 200:
            raise Exception(res.content)
        return res

    def solr_bulk_load(self, solr_core, local_file_path, sep1="\t", sep2="|") -> None:
        res = self.solr_query(
            f"""/{solr_core}/update/csv?
            stream.file={local_file_path}&stream.contentType=text/plain;charset=utf-8
            &commit=true&separator={sep1}&f.category.split=true&f.category.separator={sep2}
    """
        )
        return res

    def solr_delete_core(self, solr_core):
        res = self.solr_query(
            f"/admin/cores?action=UNLOAD&deleteInstanceDir=false&core={solr_core}"
        )
        return res

    def solr_create_core(self, solr_core):
        res = self.solr_query(
            f"""/admin/cores?action=CREATE&name={solr_core}&instanceDir={solr_core}&
            config=solrconfig.xml&schema=schema.xml&dataDir=data"""
        )
        return res
