# https://overiq.com/flask-101/database-modelling-in-flask/
import json
from datetime import datetime, timedelta
from math import ceil

from sqlalchemy.orm import aliased

from app.db import db
from app.models.statistic import StatisticModel
from app.models.user import UserModel


class ConceptModel(db.Model):
    __tablename__ = "concept"
    __table_args__ = {"schema": "susana"}

    concept_id = db.Column(db.Integer, primary_key=True)
    concept_name = db.Column(db.Text())
    domain_id = db.Column(db.Text())
    vocabulary_id = db.Column(db.Text())
    concept_class_id = db.Column(db.Text())
    standard_concept = db.Column(db.Text())
    concept_code = db.Column(db.Text())
    valid_start_date = db.Column(db.Date(), default=datetime.now())  # CURRENT_DATE
    valid_end_date = db.Column(db.Date(), default=datetime.now() + timedelta(weeks=5200))
    invalid_reason = db.Column(db.Text())
    m_language_id = db.Column(db.Text())
    m_project_id = db.Column(
        db.Integer, db.ForeignKey("susana.mapper_project.m_project_id")
    )

    relations = db.relationship(
        "ConceptRelationModel",
        backref="concept",
        lazy="dynamic",
        foreign_keys="ConceptRelationModel.concept_id_1",
    )
    relations_2 = db.relationship(
        "ConceptRelationModel",
        backref="concept_2",
        lazy="dynamic",
        foreign_keys="ConceptRelationModel.concept_id_2",
    )
    frequency = db.relationship("StatisticModel", backref="concept", lazy="dynamic")

    ancestor = db.relationship(
        "ConceptAncestorModel",
        backref="ancestor",
        lazy="dynamic",
        foreign_keys="ConceptAncestorModel.ancestor_concept_id",
    )
    descendant = db.relationship(
        "ConceptAncestorModel",
        backref="descendant",
        lazy="dynamic",
        foreign_keys="ConceptAncestorModel.descendant_concept_id",
    )

    def __init__(
        self,
        concept_id,
        concept_name,
        domain_id,
        m_language_id,
        vocabulary_id,
        concept_code,
        invalid_reason=None,
        standard_concept=None,
        valid_end_date=None,
        concept_class_id="",
        m_project_id=1,
        relations=[],
    ):
        self.concept_id = concept_id
        self.concept_name = concept_name
        self.domain_id = domain_id
        self.vocabulary_id = vocabulary_id
        self.concept_class_id = concept_class_id
        self.standard_concept = standard_concept
        self.concept_code = concept_code
        if valid_end_date != None:
            self.valid_end_date = valid_end_date
        self.invalid_reason = invalid_reason
        self.m_language_id = m_language_id
        self.m_project_id = m_project_id
        self.get_relations(relations, concept_id, m_user_id)

    def get_relations(self, relations, concept_id_1, m_user_id):
        for rel in relations:
            rel = json.loads(rel.replace("'", '"'))

            try:
                concept_id_2 = rel["concept_id"]
                relationship_id = rel["relationship_id"]
                reverse_relationship_id = rel["reverse_relationship_id"]
            except:
                {
                    "message": "you should provide concept_id, concept_name, relationship_id"
                }
            # m_algo_id is not realy 3, should add user
            rel = ConceptRelationModel(
                concept_id_1=concept_id_1,
                concept_id_2=concept_id_2,
                relationship_id=relationship_id,
                m_user_id=m_user_id,
                m_algo_id=3,
            )
            reverse_rel = ConceptRelationModel(
                concept_id_1=concept_id_2,
                concept_id_2=concept_id_1,
                relationship_id=reverse_relationship_id,
                m_user_id=m_user_id,
                m_algo_id=3,
            )
            rel.upserting()
            reverse_rel.upserting()

    def get_statistic(self, m_statistic_type_id):
        frequency = self.frequency.filter_by(
            m_statistic_type_id=m_statistic_type_id
        ).first()
        # print("frequency : ",frequency, type(frequency))
        if not frequency:
            return 0
        return frequency.m_value_as_number

    def json(self):
        ret_standard = ""
        if self.standard_concept == "S":
            ret_standard = "Standard"
        elif self.standard_concept == "C":
            ret_standard = "Classification"
        elif self.standard_concept is None:
            ret_standard = "Non-Standard"

        ret_invalid = ""
        if self.invalid_reason == "D":
            ret_invalid = "Deleted"
        elif self.invalid_reason == "U":
            ret_invalid = "Updated"
        elif self.invalid_reason is None:
            ret_invalid = "Valid"

        return {
            "concept_id": self.concept_id,
            "concept_name": self.concept_name,
            "concept_code": self.concept_code,
            "concept_class_id": self.concept_class_id,
            "vocabulary_id": self.vocabulary_id,
            "domain_id": self.domain_id,
            "invalid_reason": ret_invalid,
            "standard_concept": ret_standard,
            "m_language_id": self.m_language_id,
            "m_project_id": self.m_project_id,
            "m_frequency_id": self.get_statistic("FREQ"),
        }

    @classmethod
    def find_by_id(cls, concept_id):
        return cls.query.filter_by(concept_id=concept_id).first()

    @classmethod
    def find_concept_relation_by_id(cls, concept_id):
        concept = cls.find_by_id(concept_id)
        relations = concept.relations.all()
        if len(relations):
            liste = []
            for relation in relations:
                if relation.m_user_id is None or relation.m_modif_end_datetime is None:
                    concept_related = ConceptModel.find_by_id(relation.concept_id_2)
                    if concept_related:
                        ret_standard = ""
                        if concept_related.standard_concept == "S":
                            ret_standard = "Standard"
                        elif concept_related.standard_concept == "C":
                            ret_standard = "Classification"
                        else:
                            ret_standard = "Non-Standard"

                        ret_invalid = ""
                        if relation.invalid_reason == "D":
                            ret_invalid = "Deleted"
                        elif relation.invalid_reason == "U":
                            ret_invalid = "Update"
                        else:
                            ret_invalid = "Valid"

                        dico = {
                            # FOR the CONCEPT_id_2
                            "concept_id": relation.concept_id_2,
                            "concept_name": concept_related.concept_name,
                            "concept_code": concept_related.concept_code,
                            "concept_class_id": concept_related.concept_class_id,
                            "domain_id": concept_related.domain_id,
                            "vocabulary_id": concept_related.vocabulary_id,
                            "standard_concept": ret_standard,
                            # FOR the RELATION
                            "invalid_reason": ret_invalid,
                            "relationship_id": relation.relationship_id,
                            "m_user_id": relation.m_user_id,
                            "m_mapping_rate": relation.m_mapping_rate,
                            "m_mapping_comment": relation.m_mapping_comment,
                        }
                        # 'valid_start_date': str(relation.valid_start_date),
                        # 'valid_end_date': str(relation.valid_end_date),
                        liste.append(dico)
            return liste

    def upserting(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


class ConceptRelationModel(db.Model):
    __tablename__ = "concept_relationship"
    __table_args__ = {"schema": "susana"}

    m_concept_relationship_id = db.Column(db.Integer, primary_key=True)
    concept_id_1 = db.Column(db.Integer, db.ForeignKey("susana.concept.concept_id"))
    concept_id_2 = db.Column(db.Integer, db.ForeignKey("susana.concept.concept_id"))
    relationship_id = db.Column(db.Text())
    valid_start_date = db.Column(db.Date(), default=db.func.now())
    valid_end_date = db.Column(db.Date(), default=datetime.now() + timedelta(weeks=5200))
    invalid_reason = db.Column(db.Text())
    m_user_id = db.Column(db.Integer, db.ForeignKey("susana.mapper_user.m_user_id"))
    m_algo_id = db.Column(db.Integer)
    m_modif_start_datetime = db.Column(
        db.DateTime(), default=db.func.now()
    )  # CURRENT_DATE
    m_modif_end_datetime = db.Column(db.DateTime(), default=None)
    m_mapping_rate = db.Column(db.Integer())
    m_mapping_comment = db.Column(db.Text())

    relationship = db.relationship(
        "RelationshipModel", backref="relationship", lazy="dynamic"
    )
    user = db.relationship("UserModel", uselist=False)

    def __init__(
        self,
        concept_id_1,
        concept_id_2,
        relationship_id,
        m_user_id,
        m_algo_id,
        invalid_reason=None,
        valid_end_date=None,
        m_modif_end_datetime=None,
        m_mapping_rate=0,
        m_mapping_comment="",
    ):
        self.concept_id_1 = concept_id_1
        self.concept_id_2 = concept_id_2
        self.relationship_id = relationship_id
        if valid_end_date != None:
            self.valid_end_date = valid_end_date
        self.invalid_reason = invalid_reason
        self.m_user_id = m_user_id
        self.m_algo_id = m_algo_id
        if m_modif_end_datetime != None:
            self.m_modif_end_datetime = m_modif_end_datetime
        self.m_mapping_rate = m_mapping_rate
        self.m_mapping_comment = m_mapping_comment

    def json(self):
        data = ""
        if self.concept.project:
            data = self.concept.project.m_project_type_id
        else:
            data = "EMPTY"

        users_up = StatisticModel.find_all_users_with_one_stat(
            self.m_concept_relationship_id, "VOTE_MAPPING", "UP", None
        )
        users_down = StatisticModel.find_all_users_with_one_stat(
            self.m_concept_relationship_id, "VOTE_MAPPING", "DOWN", None
        )

        vote = 0
        if users_up:
            vote = len(users_up)
        if users_down:
            #            if type(vote) == type(""):
            #                vote = 0
            vote -= len(users_down)

        return {
            "m_concept_relationship_id": self.m_concept_relationship_id,
            "concept_id_1": self.concept_id_1,
            "concept_name_1": self.concept.concept_name,
            "domain_id_1": self.concept.domain_id,
            "vocabulary_id_1": self.concept.vocabulary_id,
            "concept_id_2": self.concept_id_2,
            "concept_name_2": self.concept_2.concept_name,
            "domain_id_2": self.concept_2.domain_id,
            "vocabulary_id_2": self.concept_2.vocabulary_id,
            "relationship_id": self.relationship_id,
            "invalid_reason": self.invalid_reason,
            "m_modif_start_datetime": str(self.m_modif_start_datetime),
            "m_modif_end_datetime": str(self.m_modif_end_datetime),
            "m_project_id": data,
            "m_username": self.user.m_username,
            "vote": vote,
            "users_up": users_up,
            "users_down": users_down,
        }

    @classmethod
    def find_all(
        cls,
        mini,
        maxi,
        available,
        project_ids,
        relationship_ids,
        order_by,
        page_shown,
        limit,
    ):
        query_set_list = []
        # numerique
        query_set = None
        if mini:
            query_set = cls.query.filter(cls.concept_id_1 >= mini)
        if maxi:
            if query_set:
                query_set = query_set.filter(cls.concept_id_1 <= maxi)
            else:
                query_set = cls.query.filter(cls.concept_id_1 <= maxi)

        if available == "last":
            if query_set:
                query_set = query_set.filter(cls.m_modif_end_datetime == None)
            else:
                query_set = cls.query.filter(cls.m_modif_end_datetime == None)
        elif available == "active":
            if query_set:
                query_set = query_set.filter(
                    cls.invalid_reason == None, cls.m_modif_end_datetime == None
                )
            else:
                query_set = cls.query.filter(
                    cls.invalid_reason == None, cls.m_modif_end_datetime == None
                )

        # list

        ConceptModelAlias = aliased(ConceptModel, name="ConceptModelAlias")
        ProjectModelAlias = aliased(ProjectModel, name="ProjectModelAlias")
        if project_ids:  # reverse condition never exist!
            # because not seen in front part (reviewerPage)
            project_ids += ["OMOP"]
            if query_set:
                # is_none =  query_set.join(ConceptModel, cls.concept_id_1==ConceptModel.concept_id).filter(ConceptModel.m_project_id == 8, cls.m_user_id != None)
                # is_allowed_project = query_set.join(ConceptModel, cls.concept_id_1==ConceptModel.concept_id).join(ProjectModel, ConceptModel.m_project_id == ProjectModel.m_project_id).filter(ProjectModel.m_project_type_id.in_(project_ids), cls.m_user_id != None)
                # query_set = is_none.union(is_allowed_project)

                query_set = (
                    query_set.join(
                        ConceptModel, cls.concept_id_1 == ConceptModel.concept_id
                    )
                    .join(
                        ProjectModel,
                        ConceptModel.m_project_id == ProjectModel.m_project_id,
                    )
                    .filter(
                        ProjectModel.m_project_type_id.in_(project_ids),
                        cls.m_user_id != None,
                    )
                )

            else:
                # is_none =  cls.join(ConceptModel, cls.concept_id_1==ConceptModel.concept_id).filter(ConceptModel.m_project_id == 8, cls.m_user_id != None)
                # is_allowed_project = cls.query.join(ConceptModel, cls.concept_id_1==ConceptModel.concept_id).join(ProjectModel, ConceptModel.m_project_id == ProjectModel.m_project_id).filter(ProjectModel.m_project_type_id.in_(project_ids), cls.m_user_id != None)
                # query_set = is_none.union(is_allowed_project)

                query_set = (
                    cls.query.join(
                        ConceptModel, cls.concept_id_1 == ConceptModel.concept_id
                    )
                    .join(
                        ProjectModel,
                        ConceptModel.m_project_id == ProjectModel.m_project_id,
                    )
                    .filter(
                        ProjectModel.m_project_type_id.in_(project_ids),
                        cls.m_user_id != None,
                    )
                )

            # is_none =  query_set.join(ConceptModel, cls.concept_id_2==ConceptModel.concept_id).filter(ConceptModel.m_project_id == 8, cls.m_user_id != None)
            # is_allowed_project = query_set.join(ConceptModel, cls.concept_id_2==ConceptModel.concept_id).join(ProjectModel, ConceptModel.m_project_id == ProjectModel.m_project_id).filter(ProjectModel.m_project_type_id.in_(project_ids), cls.m_user_id != None)
            # query_set = is_none.union(is_allowed_project)

            query_set = (
                query_set.join(
                    ConceptModelAlias, cls.concept_id_2 == ConceptModelAlias.concept_id
                )
                .join(
                    ProjectModelAlias,
                    ConceptModelAlias.m_project_id == ProjectModelAlias.m_project_id,
                )
                .filter(
                    ProjectModelAlias.m_project_type_id.in_(project_ids),
                    cls.m_user_id != None,
                )
            )

        if relationship_ids:
            if query_set:
                query_set = query_set.filter(cls.relationship_id.in_(relationship_ids))
            else:
                query_set = cls.query.filter(cls.relationship_id.in_(relationship_ids))

        offset = page_shown * limit
        count = query_set.count()
        page_found = ceil(float(count) / float(limit))

        # order, offset, limit
        if order_by == "desc" and query_set:
            query_set = (
                query_set.order_by(cls.m_modif_start_datetime.desc())
                .offset(offset)
                .limit(limit)
            )
        elif query_set:
            query_set = (
                query_set.order_by(cls.m_modif_start_datetime.asc())
                .offset(offset)
                .limit(limit)
            )

        return query_set.all(), page_shown, page_found, count

    @classmethod
    def find_by_id(cls, concept_id_1, concept_id_2=None, relationship_id=None):
        query_set = None
        if concept_id_1 != None:
            query_set = cls.query.filter_by(concept_id_1=concept_id_1)
        if concept_id_2 != None:
            if query_set != None:
                query_set = query_set.filter_by(concept_id_2=concept_id_2)
            else:
                query_set = cls.query.filter_by(concept_id_2=concept_id_2)
        if relationship_id != None:
            if query_set != None:
                query_set = query_set.filter_by(relationship_id=relationship_id)
            else:
                query_set = cls.query.filter_by(relationship_id=relationship_id)
        if query_set != None:
            query_set = query_set.order_by(cls.m_modif_start_datetime.desc())
        return query_set.all()

    @classmethod
    def find_by_concept_relationship_id(cls, m_concept_relationship_id):
        return cls.query.filter_by(
            m_concept_relationship_id=m_concept_relationship_id
        ).first()

    def upserting(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def copy_all_ConceptRelation(cls, old_id, new_id):
        query_relation_1 = ConceptRelationModel.find_by_id(old_id)
        query_relation_2 = ConceptRelationModel.find_by_id(
            concept_id_1=None, concept_id_2=old_id
        )
        for q in query_relation_1:
            if new_id != q.concept_id_2:
                rel = ConceptRelationModel(
                    new_id, q.concept_id_2, q.relationship_id, q.m_user_id, q.m_algo_id
                )
                db.session.add(rel)

        for q in query_relation_2:
            if new_id != q.concept_id_1:
                rel = ConceptRelationModel(
                    q.concept_id_1, new_id, q.relationship_id, q.m_user_id, q.m_algo_id
                )
                db.session.add(rel)
        db.session.commit()

    @classmethod
    def updated_from(cls, old_id, new_id):
        # algo_id = 6 == ici 'modify by user'
        rel = ConceptRelationModel(
            old_id, new_id, "Concept replaced by", m_user_id=1, m_algo_id=6
        )
        db.session.add(rel)

        rev_rel = ConceptRelationModel(
            new_id, old_id, "Concept replaces", m_user_id=1, m_algo_id=6
        )
        db.session.add(rev_rel)
        db.session.commit()


class RelationshipModel(db.Model):
    __tablename__ = "relationship"
    __table_args__ = {"schema": "susana"}

    relationship_id = db.Column(
        db.Text(),
        db.ForeignKey("susana.concept_relationship.relationship_id"),
        primary_key=True,
    )
    relationship_name = db.Column(db.Text())
    is_hierarchical = db.Column(db.Text())
    defines_ancestry = db.Column(db.Text())
    reverse_relationship_id = db.Column(db.Text())
    relationship_concept_id = db.Column(db.Integer)

    @classmethod
    def find_by_id(cls, relationship_id):
        return cls.query.filter_by(relationship_id=relationship_id).first()


class ConceptAncestorModel(db.Model):
    __tablename__ = "concept_ancestor"
    __table_args__ = {"schema": "susana"}

    ancestor_concept_id = db.Column(
        db.Integer, db.ForeignKey("susana.concept.concept_id"), primary_key=True
    )
    descendant_concept_id = db.Column(
        db.Integer, db.ForeignKey("susana.concept.concept_id"), primary_key=True
    )
    min_levels_of_separation = db.Column(db.Integer)
    max_levels_of_separation = db.Column(db.Integer)

    @classmethod
    def find_descendant_by_id(cls, ancestor_concept_id):
        descendant = []
        # limit 200 because concept have >200 000 infants
        for elem in cls.query.filter_by(ancestor_concept_id=ancestor_concept_id).limit(
            200
        ):
            concept = elem.descendant
            if concept:
                descendant.append(concept.json())
        return descendant

    @classmethod
    def find_ancestor_by_id(cls, descendant_concept_id):
        ancestor = []
        for elem in cls.query.filter_by(
            descendant_concept_id=descendant_concept_id
        ).limit(200):
            concept = elem.ancestor
            if concept:
                ancestor.append(concept.json())
        return ancestor


class ProjectModel(db.Model):
    __tablename__ = "mapper_project"
    __table_args__ = {"schema": "susana"}

    m_project_id = db.Column(db.Integer, primary_key=True)
    m_project_type_id = db.Column(db.Text(255), primary_key=True)
    m_valid_start_datetime = db.Column(db.Date(), default=datetime.now())  # CURRENT_DATE
    m_valid_end_datetime = db.Column(
        db.Date(), default=datetime.now() + timedelta(weeks=5200)
    )
    m_invalid_reason = db.Column(db.Text())

    concepts = db.relationship(
        "ConceptModel",
        backref="project",
        lazy="dynamic",
        foreign_keys="ConceptModel.m_project_id",
    )
    roles = db.relationship(
        "RoleModel",
        backref="project",
        lazy="dynamic",
        foreign_keys="RoleModel.m_project_id",
    )

    def json(self):
        return {
            "m_project_id": self.m_project_id,
            "m_project_type_id": self.m_project_type_id,
        }

    @classmethod
    def find_by_id(cls, m_project_id):
        return cls.query.filter_by(m_project_id=m_project_id).first()

    @classmethod
    def find_by_name(cls, m_project_type_id):
        return cls.query.filter_by(m_project_type_id=m_project_type_id).first()

    @classmethod
    def valid_projects(cls):
        return (
            cls.query.distinct(cls.m_project_type_id)
            .filter_by(m_invalid_reason=None)
            .all()
        )


class RoleModel(db.Model):
    __tablename__ = "mapper_role"
    __table_args__ = {"schema": "susana"}

    m_project_id = db.Column(
        db.Integer,
        db.ForeignKey("susana.mapper_project.m_project_id"),
        primary_key=True,
    )
    m_user_id = db.Column(db.Integer, primary_key=True)
    m_role_id = db.Column(db.Text(255), primary_key=True)
    m_valid_start_datetime = db.Column(db.Date(), default=datetime.now())  # CURRENT_DATE
    m_valid_end_datetime = db.Column(
        db.Date(), default=datetime.now() + timedelta(weeks=5200)
    )
    m_invalid_reason = db.Column(db.Text())

    def __init__(
        self,
        m_project_id,
        m_user_id,
        m_role_id,
        m_valid_end_datetime=None,
        m_invalid_reason=None,
    ):
        self.m_project_id = m_project_id
        self.m_user_id = m_user_id
        self.m_role_id = m_role_id
        if m_valid_end_datetime != None:
            self.m_valid_end_datetime = m_valid_end_datetime
        self.m_invalid_reason = m_invalid_reason

    def upserting(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_roles_by_username(cls, m_username):
        user = UserModel.find_by_username(m_username)
        if user:
            return cls.query.filter_by(m_user_id=user.id).all()
        return []

    # one user, one role_id => projects_id allowed
    @classmethod
    def projects_id_allowed(cls, m_user_id, m_role_id=None):
        if m_role_id is not None:
            roles = cls.query.filter_by(m_user_id=m_user_id, m_role_id=m_role_id).all()
        else:
            roles = cls.query.filter_by(m_user_id=m_user_id).all()
        return [role.m_project_id for role in roles]

    # one user, one role_id => projects_type_id (string) allowed
    @classmethod
    def projects_type_id_allowed(cls, m_user_id, m_role_id=None):
        if m_role_id is not None:
            roles = cls.query.filter_by(m_user_id=m_user_id, m_role_id=m_role_id).all()
        else:
            roles = cls.query.filter_by(m_user_id=m_user_id).all()
        return [role.project.m_project_type_id for role in roles]

    @classmethod
    def projects_roles_allowed(cls, m_user_id, m_role_id=None):
        if m_role_id is not None:
            roles = cls.query.filter_by(m_user_id=m_user_id, m_role_id=m_role_id).all()
        else:
            roles = cls.query.filter_by(m_user_id=m_user_id).all()
        return [role.project.m_project_type_id + "-" + role.m_role_id for role in roles]

    @classmethod
    def read(cls, concept_id, m_user_id):
        concept = ConceptModel.find_by_id(concept_id)
        if concept:
            if (
                concept.m_project_id
                and concept.m_project_id not in cls.projects_id_allowed(m_user_id, "READ")
            ):
                return False
        return True

    @classmethod
    def write(cls, concept_id, m_user_id):
        concept = ConceptModel.find_by_id(concept_id)
        if concept:
            if (
                concept.m_project_id
                and concept.m_project_id
                not in cls.projects_id_allowed(m_user_id, "WRITE")
            ):
                return False
        return True

    @classmethod
    def delete(cls, concept_id, m_user_id):
        concept = ConceptModel.find_by_id(concept_id)
        if concept:
            if (
                concept.m_project_id
                and concept.m_project_id
                not in cls.projects_id_allowed(m_user_id, "DELETE")
            ):
                return False
        return True

    @classmethod
    def vote(cls, m_concept_relationship_id, m_user_id):
        concept_relationship = ConceptRelationModel.find_by_concept_relationship_id(
            m_concept_relationship_id
        )
        concept_1 = concept_relationship.concept
        concept_2 = concept_relationship.concept_2
        if concept_1 and concept_2:
            if (
                concept_1.m_project_id
                and concept_1.m_project_id
                not in cls.projects_id_allowed(m_user_id, "VOTE")
            ):
                return False
            if (
                concept_2.m_project_id
                and concept_2.m_project_id
                not in cls.projects_id_allowed(m_user_id, "VOTE")
            ):
                return False
        return True
