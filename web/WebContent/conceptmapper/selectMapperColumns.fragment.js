sap.ui.jsfragment("conceptmapper.selectMapperColumns", {
	
	createContent: function(oController) {

		var oColumsDialog = new sap.m.Dialog({
			title : "{i18n>select_columns}",
			icon : "sap-icon://customize",
		});
		
		oColumsDialog.addContent(
				new sap.m.List({
					id: "selectColumnsList",
					  mode: sap.m.ListMode.MultiSelect, 
					  setGrowingScrollToLoad: true,
					  items : [
						  
						  	new sap.m.StandardListItem({
								title: "{i18n>concept_id}",
								selected: true,
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_name}",
								selected: true,
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_code}",
								selected: true,

							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_class_id}",
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>vocabulary_id}",
								selected: true,
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>domain_id}",
								selected: true,
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>standard_concept}",
							}),
							
							
							
							new sap.m.StandardListItem({
								title: "{i18n>frequency}",
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>score}",
								selected: true,

							}),
						  
						  
							
							
					  ]
			
					  
					})
		);
		
		oColumsDialog.addButton(
                new sap.m.Button({
                    text: "OK",
					icon: "sap-icon://accept",
					type: sap.m.ButtonType.Accept,

					
                    press: function () {
                    	var columnsList = sap.ui.getCore().byId("selectColumnsList");
                    	sap.ui.getCore().byId("conceptIdMapper").setVisible(columnsList.getAggregation('items')[0].getProperty('selected'));
                    	sap.ui.getCore().byId("conceptNameMapper").setVisible(columnsList.getAggregation('items')[1].getProperty('selected'));
                    	sap.ui.getCore().byId("conceptCodeMapper").setVisible(columnsList.getAggregation('items')[2].getProperty('selected'));
                    	sap.ui.getCore().byId("conceptClassMapper").setVisible(columnsList.getAggregation('items')[3].getProperty('selected'));
                    	sap.ui.getCore().byId("vocabularyMapper").setVisible(columnsList.getAggregation('items')[4].getProperty('selected'));
                    	sap.ui.getCore().byId("standardMapper").setVisible(columnsList.getAggregation('items')[6].getProperty('selected'));
                    	sap.ui.getCore().byId("domainMapper").setVisible(columnsList.getAggregation('items')[5].getProperty('selected'));
                    	sap.ui.getCore().byId("frequencyMapper").setVisible(columnsList.getAggregation('items')[7].getProperty('selected'));
                    	sap.ui.getCore().byId("scoreMapper").setVisible(columnsList.getAggregation('items')[8].getProperty('selected'));


                    	oColumsDialog.close();
					}
                })
        );

		
		
		return oColumsDialog;
		
	}
});